{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1b625925-9778-467a-bad8-e4d68f35c4ad",
   "metadata": {},
   "source": [
    "# Hypothesis tests\n",
    "\n",
    "We will introduce Hypothesis tests looking at the example of a counting experiment with background uncertainty. \n",
    "\n",
    "## p-value for discovery of a new signal\n",
    "\n",
    "In searches for new physics we want to know how significant a potential deviation from our Standard Model (SM) expectation is. We do this by a hypothesis test where we try to exclude the SM (\"background only\") hypothesis. We use a so called **p-value** $p_0$ for this, abstractly defined by:\n",
    "\n",
    "$$p_0 = \\int\\limits_{t_\\mathrm{obs}}^{\\infty}p(t|H_0)\\mathrm{d}t$$\n",
    "\n",
    "where $t$ is a test statistic (a number we calculate from our data observations) and $p(t|H_0)$ is the probability distribution for $t$ under the assumption of our **null Hypothesis** $H_0$, in this case the background only hypothesis. This p-value is then typically converted into a number of standard deviations $z$, the **significance** (\"number of sigmas\") via the inverse of the cumulative standard normal distribution $\\Phi$:\n",
    "\n",
    "$$z = \\Phi^{-1}(1 - p)$$\n",
    "\n",
    "The typical convention for particle physics is to speak of *evidence* when $z>3$ and of an *observation* when $z>5$.\n",
    "\n",
    "So what do we use for $t$? We want to use something that discriminates well between our null Hypothesis and an **alternative Hypothesis** that we have in mind. When we try to discover new physics, our null Hypothesis is the absence and the alternative Hypothesis the presence of a signal. We can parametrize this by a **signal strength** parameter $\\mu$. The test statistics used in almost all LHC searches use the **profile likelihood ratio**\n",
    "\n",
    "$$\\Lambda_\\mu = \\frac{L(\\mu, \\hat{\\hat{\\theta}})}{L(\\hat{\\mu}, \\hat{\\theta})}$$\n",
    "\n",
    "where $\\theta$ are the other parameters of our model that are not part of the test, the so called **nuisance parameters**. In contrast, the parameter that we want to test, $\\mu$, is called our **parameter of interest** (POI). The nuisance parameters include all fit parameters, like normalization factors and parameters for describing uncertainties. $L(\\mu, \\hat{\\hat{\\theta}})$ is the Likelihood function, maximized under the condition that our parameter of interest takes the value $\\mu$ and $L(\\hat{\\mu}, \\hat{\\theta})$ is the unconditionally maximized Likelihood. So roughly speaking, we are calculating the fraction of the maximum possible likelihood that we can get under our test condition. If it is high, that speaks for our hypothesis, if it is low, against. The test statistic $t_\\mu$ is then defined as\n",
    "\n",
    "$$t_\\mu = -2\\ln\\Lambda_\\mu$$\n",
    "\n",
    "giving us a test statistic where **high values speak against the null hypothesis**.\n",
    "\n",
    "<div class=\"alert alert-block alert-success\">\n",
    "    <b>Question 7:</b> If we want to discover a new signal (using the p-value $p_0$), which value of $\\mu$ are we testing against? Or in other words, what is our null Hypothesis?\n",
    "</div>\n",
    "\n",
    "All that's left now is to know the distribution of $p(t_\\mu|H_0)$. [Wilk's theorem](https://en.wikipedia.org/wiki/Wilks%27_theorem) tells us that the distribution of $t_\\mu$ is asymptotically (for large sample sizes) a chi-square distribution with degrees of freedom parameter equal to the difference of number of free parameters in the alternative (denominator) hypothesis and the null (numerator) hypothesis, in this case 1 since we have one parameter of interest and the number of nuisance parameters is the same for both Hypothesis. For the discovery p-value we use a slightly modified version of test statistic, called $q_0$ where $\\hat{\\mu}$ is required to be $>=0$ ($q_0=0$ for $\\hat{\\mu} < 0$). For $q_0$ the p-value in the asymptotic limit collapses to a very simple formula:\n",
    "\n",
    "$$p_0 = \\sqrt{q_0}$$\n",
    "\n",
    "The asymptotic limit often matches quite well even for fairly small sample sizes, but it should be kept in mind this is an approximation. Alternatively, one can evaluate $p(t_\\mu|H_0)$ by Monte Carlo sampling (\"toys\").\n",
    "\n",
    "## CLs for exclusion of an absent signal\n",
    "\n",
    "Now, sadly, not all searches find evidence for new physics. What we still can do in such a case is to try exclude models by rejecting the hypothesis of a signal being present. That usually means we test against $\\mu=1$ or some other value $>0$. The rest of the procedure is very similar with one small detail worth mentioning ... In high energy physics it is very common to use a quantity called $CL_s$ instead of plain p-value. It is defined by\n",
    "\n",
    "$$CL_s = \\frac{CL_{s+b}}{CL_{b}}$$\n",
    "\n",
    "where $CL_{s+b}$ is the p-value for rejecting the hypothesis of signal + background being present (what would be the \"normal\" p-value) and $CL_{b}$ is the p-value for rejecting the background only hypothesis, but now using the test statistic for $\\mu=1$ (so this is different from $p_0$!). We won't go into further details how to calculate those p-values.\n",
    "\n",
    "The asymptotic distributions for all different variants are described in the paper \"Asymptotic formulae for likelihood-based tests of new physics\" ([arXiv:1007.1727](https://arxiv.org/abs/1007.1727)).\n",
    "\n",
    "Just a qualitative explanation of why we use $CL_s$ instead of the p-value: We want to avoid excluding signals in cases where we don't have sensitivity, but observe an *underfluctuation* of the data. In these cases $CL_{s+b}$ and $CL_b$ will be very similar and consequently lead to a large value of $CL_{s}$, telling us the signal is **not** excluded. In case our observations are exactly on spot with the background expectations $CL_b = 0.5$ in the asymptotic limit, so on average we have twice as high \"p-values\" with $CL_s$.\n",
    "\n",
    "The typical convention for particle physics is to speak of an **exclusion** of a signal if $CL_s < 0.05$. That's usually what is meant by \"limit at 95% confidence level\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67f9b194-0707-4b8b-bc2e-80ebca47fa47",
   "metadata": {},
   "source": [
    "## Introduction and reminders"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c2aeaf5-451a-481f-8584-99ba9b4d94c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.optimize import minimize\n",
    "from scipy import stats"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d96c0ca9-26ce-49ca-ad35-203ddde8b30a",
   "metadata": {},
   "source": [
    "Converting p-values and significances (number of standard deviations $z$) is done using the inverse of the cumulative standard normal distribution $\\Phi$\n",
    "\n",
    "$$z = \\Phi^{-1}(1 - p)$$\n",
    "\n",
    "The function [`scipy.stats.norm.isf`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.norm.html#scipy.stats.norm) (\"inverse survival function\") calculates $\\Phi^{-1}(1 - p)$ in a numerically stable way (also for small p-values)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e6ab91c-64fd-454e-a7b1-b98886a0238b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def pvalue_to_significance(pvalue):\n",
    "    return stats.norm.isf(pvalue)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f21f6436-5069-454f-a43f-7b86ce3d4304",
   "metadata": {},
   "source": [
    "The other direction is given by\n",
    "\n",
    "$$p = 1 - \\Phi(z)$$\n",
    "\n",
    "which is provided numerically stable by `scipy.stats.norm.isf` (\"survival function\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fcad8f9c-f581-49e8-8c5d-3425f8e9e1bf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def significance_to_pvalue(significance):\n",
    "    return stats.norm.sf(significance)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c0ae73c-bd6a-4723-a4ef-3a73e4280dbd",
   "metadata": {},
   "outputs": [],
   "source": [
    "[(z, significance_to_pvalue(z)) for z in range(6)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97090593-d55c-42f2-ab78-8326dc13e91f",
   "metadata": {},
   "source": [
    "**Note:** This conversion assumes *one-sided* p-values - integrating in direction of the tail of one side of a normal distribution. *Two-sided* p-values are twice as large for the same significance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1e84641c-10fd-4edb-8f8b-b659eb26b954",
   "metadata": {},
   "outputs": [],
   "source": [
    "[(z, 2*significance_to_pvalue(z)) for z in range(6)]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3f05c06-b6cf-4b97-a04b-dd0b98e883e0",
   "metadata": {},
   "source": [
    "Let's look at a simple counting experiment where we know the expected number of background events and assume a Poisson distribution\n",
    "\n",
    "<div class=\"alert alert-block alert-success\">\n",
    "    <b>Question 8:</b> We expect 5 background events (our null Hypothesis). How many events would we need to observe to reject the null hypothesis with at least 3 (evidence) and 5 (observation) sigma? Our test statistic is the total number of events observed and we count more events than the expectation as evidence against the null hypothesis.<br><br>\n",
    "    <b>Hint:</b> Iterate through the number of events observed, starting from 5 upwards and use <code>1 - stats.poisson.cdf(...)</code> to calculate the p-value.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b9bb8cd-588c-4370-b5a8-8dc835a44c6a",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <b>Question 9:</b> If we turn the hypotheses around and now want to exclude the presence of a signal - what is the smallest upper limit at 95% confidence level (p-value smaller than 0.05) on the number of signal events we can possibly get from a counting experiment? Here our Null hypothesis would be the presence of a signal and we count fewer events than observed as evidence against it.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aca8b4da-8049-495d-ba4f-bd2caaa3d81d",
   "metadata": {},
   "source": [
    "## Discovery or exclusion of a signal for a counting experiment with background uncertainty (the \"on-off\" problem)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2274fb9-a1df-4fb1-a3a2-e3f380f60f45",
   "metadata": {},
   "source": [
    "Let's now go again to the situation where we have an uncertainty on the background model. How do we incorporate this? If we do Bayesian statistics we could regard the uncertainty as our prior on the background expectation, assume some other prior on the signal and get the posterior of the signal given the data.\n",
    "\n",
    "Here we want to investigate the Frequentist method. What does \"uncertainty\" on the background mean in this case? In Frequentist statistics only the observations are random variables, but the parameters of our model are fixed Hypothesis. A typical way to incorporate an uncertainty on a parameter is to view this as determined from an additional (\"auxiliary\") measurment. For our case of background uncertainty we could for example assume we look now at 2 counts, one in a region where we have background and signal (\"on\"), and another one where we have only background (\"off\") and assume we know the ratio $\\tau$ of background events in the two regions. This is perfectly applicable to many real situations, e.g.\n",
    "\n",
    "* Our background uncertainty may come from a sideband measurement, e.g. we have a peaking signal where we can look on-peak and off-peak.\n",
    "* We may have determined the background from a Monte-Carlo (MC) sample where we know the shape (therefore ratio $\\tau$), but not the normalization.\n",
    "\n",
    "The likelihood function for this can be formulated as a primary measurement of `n_on` events and a control (\"auxiliary\") measurment of `n_off` events that constrains our background parameter within the uncertainty. So, a product of 2 Poisson distributions:\n",
    "\n",
    "$$L(s, b) = \\mathrm{Pois}(n_\\mathrm{on}|s + b)\\cdot \\mathrm{Pois}(n_\\mathrm{off}|\\tau b)$$\n",
    "\n",
    "The parameter $\\tau$ can be given in terms of our background uncertainty $\\sigma_b$ by asking the question \"How much more events do i have to measure in the control region to get the relative uncertainty $\\sigma_b / b$\". That gives\n",
    "\n",
    "$\\tau = \\frac{b}{\\sigma_b^2}$\n",
    "\n",
    "You already know from the lecture how to get the negative log-likelihood for a histogram with poisson counts (here we will have a histogram with only 2 bins):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c811a942-b791-417c-a876-9a59047bb25c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def nll_hist(obs, exp):\n",
    "    \"\"\"\n",
    "    Negative log-likelihood for histogram with poisson distributed counts (up to constant terms)\n",
    "    \"\"\"\n",
    "    return np.sum(exp, axis=0) - np.sum(obs * np.log(exp), axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c807d34a-83cd-427f-af18-2d4405828f85",
   "metadata": {},
   "source": [
    "Let's assume the following situation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6906bcf4-1bf5-4c59-89c4-cbe02c35f00f",
   "metadata": {},
   "outputs": [],
   "source": [
    "b = 5\n",
    "delta_b = 2\n",
    "\n",
    "nobs = 11"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "674679b5-682e-43c0-b158-3009c701da86",
   "metadata": {},
   "source": [
    "What's the p-value for rejecting the null hypothesis of no signal?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17981cc8-28be-4ea4-915e-e8f9a9664f4f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tau_from_db(b, db):\n",
    "    \"\"\"\n",
    "    Calculate tau (the ratio between expected background in the off and on region)\n",
    "    from the expected background and the absolute uncertainty on it.\n",
    "    \"\"\"\n",
    "    return b / (db ** 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3c59b52-b544-4438-8c17-0b95ba1c7db8",
   "metadata": {},
   "outputs": [],
   "source": [
    "non = nobs\n",
    "tau = tau_from_db(b, delta_b)\n",
    "noff = tau * b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a79e5b7b-0803-4140-a159-f785a40e6837",
   "metadata": {},
   "outputs": [],
   "source": [
    "def nll_onoff(non, noff, s, b, tau):\n",
    "    obs = np.array([non, noff])\n",
    "    exp = np.array([s + b, tau * b])\n",
    "    return nll_hist(obs, exp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "175d6868-ccff-449e-82cb-2f528f6d10bb",
   "metadata": {},
   "outputs": [],
   "source": [
    "nll_onoff(non, noff, s=0, b=b, tau=tau)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b44d4f6c-4b3d-4f39-921a-34b68357e668",
   "metadata": {},
   "source": [
    "We will use the (profile) likelihood ratio test statistic to quantify this.\n",
    "\n",
    "Our **alternative hypothesis** (H1) is given by an **unconditional fit**, allowing signal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab87f513-4e83-4c39-bc9a-794a9eefa1a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "minimize(lambda pars: nll_onoff(non, noff, s=pars[0], b=pars[1], tau=tau), (1, 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b579fe6a-5bdc-4efb-a7eb-f5e663cb7dcc",
   "metadata": {},
   "source": [
    "This makes sense - we fit 6 signal events and 5 background events, matching the 11 observations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5002b27-459b-4a42-aadf-540532d7bb57",
   "metadata": {},
   "source": [
    "The **null hypothesis** (H0) is given by a **conditional fit, assuming 0 signal**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b68421e5-0f50-4ad2-a127-ada8aa0c99e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "minimize(lambda pars: nll_onoff(non, noff, s=0, b=pars[0], tau=tau), (1,))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcdecc42-6ab0-426a-9457-688e2365340c",
   "metadata": {},
   "source": [
    "The background gets slightly \"pulled\" up from the expectation of 5, but disagreement with observations remains since it is constrained by the \"off\" region. The amount of allowed \"pull\" is given by the uncertainty."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae884bfb-ef2c-4814-ab2c-01a8ab88352a",
   "metadata": {},
   "source": [
    "This setup has the nice property that we don't need to do a fit since we can find the maximum likelihood estimates (MLEs) analytically:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "866d65e4-3b17-47a8-ac5b-43cf221dc3a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy\n",
    "from sympy.solvers import solve"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b13484d-898b-46c1-bce3-31d2fe463093",
   "metadata": {},
   "outputs": [],
   "source": [
    "non_, noff_, s_, b_, tau_ = sympy.symbols(\"n_on n_off s b tau\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "137847ae-51f1-4a3d-a8bb-1bac2352518b",
   "metadata": {},
   "outputs": [],
   "source": [
    "nll = (s_ + b_) + (tau_ * b_) - (non_ * sympy.log(s_ + b_) + noff_ * sympy.log(tau_ * b_))\n",
    "nll"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86a5e053-78d8-4174-aa6b-bf903d005af7",
   "metadata": {},
   "outputs": [],
   "source": [
    "s_hat = solve(sympy.diff(nll, s_), s_)[0]\n",
    "s_hat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d60d09ed-da22-4fb9-93b6-40d3f67eb3b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "b_hat = solve(sympy.diff(nll.subs(s_, s_hat), b_), b_)[0]\n",
    "b_hat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ece6bca8-7d55-4857-bf8d-a4a24f3db204",
   "metadata": {},
   "source": [
    "These two are intuitively very clear - without constraint the best-fit signal yield will just be the total number of \"on\" events minus the number of expected background and the background will be exclusively determined from the \"off\" region.\n",
    "\n",
    "The best-fit background for a fixed signal is less clear and we will get 2 solutions for the quadratic equation that results from setting the derivative to 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "769acf7f-f044-4e59-96f7-966054bb5927",
   "metadata": {},
   "outputs": [],
   "source": [
    "b_hathat = solve(sympy.diff(nll, b_), b_)\n",
    "display(b_hathat[0])\n",
    "display(b_hathat[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77d76207-145c-4ed8-8ade-cb8c77c7d841",
   "metadata": {},
   "source": [
    "Here we only need the case for `s=0`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0685affa-a20c-4553-b03e-28c4fe48f565",
   "metadata": {},
   "outputs": [],
   "source": [
    "display(b_hathat[0].subs(s_, 0))\n",
    "display(b_hathat[1].subs(s_, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4c2db36-22e8-4ebe-8981-db1ffa4d8465",
   "metadata": {},
   "source": [
    "We can simplify the expression under the square root and see that the first solution is 0 which is not a useful estimate, so we only need the second solution.\n",
    "\n",
    "The relevant MLEs to get the log-likelihood ratio test statistic are therefore:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ee040dd-9c64-417c-a325-66f56a2a2169",
   "metadata": {},
   "outputs": [],
   "source": [
    "def mles(non, noff, b, tau):\n",
    "    \"Maximum likelihood estimates for the on-off likelihood\"\n",
    "    shat = non - b\n",
    "    bhat = noff / tau\n",
    "    bhathat = (noff + non) / (tau + 1)\n",
    "    return shat, bhat, bhathat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ffd97e5d-675d-491e-ad8e-e71a992c130e",
   "metadata": {},
   "outputs": [],
   "source": [
    "mles(non, noff, b, tau) # consistent with fit above"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b3c5901-2183-4d19-9607-5a169fe9186f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def nllr(non, noff, tau):\n",
    "    \"\"\"\n",
    "    Negative log likelihood ratio for on-off problem\n",
    "    \"\"\"\n",
    "    b = noff / tau\n",
    "    shat, bhat, bhathat = mles(non, noff, b, tau)\n",
    "\n",
    "    cond_nll = nll_onoff(non, noff, s=0, b=bhathat, tau=tau)\n",
    "    uncond_nll = nll_onoff(non, noff, s=shat, b=bhat, tau=tau)\n",
    "\n",
    "    return np.where(\n",
    "        (shat <= 0) | (bhat <= 0),\n",
    "        0, # we choose to view lower counts as background expectation not as evidence against signal\n",
    "        cond_nll - uncond_nll\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a69b1c5-bb6d-4f1f-a516-9a64d25aff2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "q0_obs = 2 * nllr(non, noff, tau)\n",
    "q0_obs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2524ecc-8b93-4d33-988a-8d0d2465036a",
   "metadata": {},
   "source": [
    "Is this significant? Let's throw toys under null hypothesis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8b4d4b1c-ce3d-42ca-8b51-fb25df73c3e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_toys = 100000\n",
    "toys = 2 * nllr(np.random.poisson(b, size=n_toys), np.random.poisson(tau * b, size=n_toys), tau)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24c01474-71c0-445c-bd2f-d0ca419124e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(toys, bins=100, label=\"expected under $H_0$\");\n",
    "plt.axvline(q0_obs, color=\"red\", label=\"observed\")\n",
    "plt.yscale(\"log\")\n",
    "plt.xlabel(\"Test statistic $q_0$\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3688a70-cf7e-45af-8697-66c42f640fa9",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <b>Question 10:</b> Using the toy data - what is the p-value?\n",
    "</div>\n",
    "\n",
    "<div class=\"alert alert-block alert-success\">\n",
    "    <b>Question 11:</b> How well does this agree with the asymptotic distribution (chi squared distribution with one degree of freedom, scaled by a factor of 0.5). What is the p-value from the asymptotic formula? Have a look at the Introduction for the formula.\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
